CREATE TABLESPACE USER_DATA DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_USER_DATA_1.dbf'
  SIZE 10M AUTOEXTEND OFF
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE TABLESPACE USER_INDEX DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_USER_INDEX_1.dbf'
  SIZE 10M AUTOEXTEND OFF
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE TABLESPACE LOB_DATA DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_LOB_DATA_1.dbf'
  SIZE 10M AUTOEXTEND OFF
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE TABLE Products (
    ProductID NUMBER(10) PRIMARY KEY,
    Name VARCHAR2(50),
    Price NUMBER(10,2),
    Inventory NUMBER(10)
) TABLESPACE USER_DATA;

CREATE TABLE Orders (
    OrderID NUMBER(10) PRIMARY KEY,
    UserID NUMBER(10),
    OrderTime DATE,
    OrderState NUMBER(2)
) TABLESPACE USER_DATA;
CREATE TABLE Users (
    UserID NUMBER(10) PRIMARY KEY,
    UserName VARCHAR2(50),
    Password VARCHAR2(50),
    Contact VARCHAR2(50)
) TABLESPACE USER_DATA;

CREATE TABLE Addresses (
    AddressID NUMBER(10) PRIMARY KEY,
    UserID NUMBER(10),
    ContactName VARCHAR2(50),
    Address VARCHAR2(100)
) TABLESPACE USER_DATA;
CREATE TABLE OrderDetails (
OrderDetailID NUMBER(10) PRIMARY KEY,
OrderID NUMBER(10),
ProductID NUMBER(10),
Quantity NUMBER(10)
) TABLESPACE USER_DATA;

CREATE USER SELLER IDENTIFIED BY password1 DEFAULT TABLESPACE USER_DATA TEMPORARY TABLESPACE TEMP;

GRANT INSERT, SELECT, UPDATE, DELETE ON Products TO SELLER;
GRANT INSERT, SELECT, UPDATE, DELETE ON Orders TO SELLER;
GRANT INSERT, SELECT, UPDATE, DELETE ON Addresses TO SELLER;

GRANT CREATE PROCEDURE TO SELLER;
GRANT CREATE FUNCTION TO SELLER;

-- 创建BUYER用户
CREATE USER BUYER IDENTIFIED BY password2 DEFAULT TABLESPACE USER_DATA TEMPORARY TABLESPACE TEMP;

-- 给BUYER赋予订单表和地址信息表的读写权限
GRANT INSERT, SELECT, UPDATE, DELETE ON Orders TO BUYER;
GRANT INSERT, SELECT, UPDATE, DELETE ON Addresses TO BUYER;
-- 创建订单的存储过程
CREATE OR REPLACE PROCEDURE CreateOrder
(
    iUserID IN NUMBER,
    iProductID IN NUMBER,
    iOrderQuantity IN NUMBER
)
AS
BEGIN
    -- 更新商品信息表
    UPDATE Products SET Inventory = Inventory - iOrderQuantity WHERE ProductID = iProductID;
    
    -- 插入订单信息
    INSERT INTO Orders (OrderID, UserID, OrderTime, OrderState) VALUES (1, iUserID, SYSDATE, 1);
END;

-- 取消订单的存储过程
CREATE OR REPLACE PROCEDURE CancelOrder
(
    iOrderID IN NUMBER
)
AS
    vOrderState NUMBER(2);
    vProductID NUMBER(10);
    vOrderQuantity NUMBER(10);
BEGIN
    -- 查询订单状态和商品信息
    SELECT OrderState INTO vOrderState FROM Orders WHERE OrderID = iOrderID;
    SELECT ProductID, Quantity INTO vProductID, vOrderQuantity FROM OrderDetails WHERE OrderID = iOrderID;
    
    -- 判断订单是否可取消并更新订单状态和商品信息
    IF vOrderState = 2 THEN
        UPDATE Orders SET OrderState = 3 WHERE OrderID = iOrderID;
        UPDATE Products SET Inventory = Inventory + vOrderQuantity WHERE ProductID = vProductID;
    END IF;
END;

DECLARE
    v_product_id NUMBER(10);
    v_order_id NUMBER(10);
    v_user_id NUMBER(10);
    v_address_id NUMBER(10);
    v_details_id NUMBER(10);
BEGIN
    -- 插入商品信息
    FOR i IN 1..10000 LOOP
        v_product_id := i;
        INSERT INTO Products(ProductID, Name, Price, Inventory)
        VALUES(v_product_id, 'Product ' || i, TRUNC(DBMS_RANDOM.VALUE(10, 100), 2), TRUNC(DBMS_RANDOM.VALUE(100, 1000)));
    END LOOP;
    
    -- 插入用户信息
    FOR i IN 1..2000 LOOP
        v_user_id := i;
        INSERT INTO Users(UserID, UserName, Password, Contact)
        VALUES(v_user_id, 'User ' || i, 'password', 'contact');
    END LOOP;
    
    -- 插入地址信息
    FOR i IN 1..4000 LOOP
        v_address_id := i;
        INSERT INTO Addresses(AddressID, UserID, ContactName, Address)
        VALUES(v_address_id, TRUNC(DBMS_RANDOM.VALUE(1, 2000)), 'Address ' || i, 'Address ' || i);
    END LOOP;
    
    -- 插入订单信息和订单详细信息
    FOR i IN 1..100000 LOOP
        v_order_id := i;
        v_details_id :=i;
        INSERT INTO Orders(OrderID, UserID, OrderTime, OrderState)
        VALUES(v_order_id, TRUNC(DBMS_RANDOM.VALUE(1, 2000)), SYSDATE - TRUNC(DBMS_RANDOM.VALUE(0, 365)), TRUNC(DBMS_RANDOM.VALUE(1, 3)));
--        FOR j IN 1..TRUNC(DBMS_RANDOM.VALUE(1, 5)) LOOP
--            INSERT INTO OrderDetails(OrderDetailID,OrderID, ProductID, Quantity)
--            VALUES(v_details_id,v_order_id, TRUNC(DBMS_RANDOM.VALUE(1, 10000)), TRUNC(DBMS_RANDOM.VALUE(1, 10)));
--        END LOOP;  
    END LOOP;
END;
