﻿﻿﻿﻿

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 数据库设计

- 表空间设计方案：

1. USER_DATA： 存放用户数据及索引，包括商品信息表，订单表，用户信息表和地址信息表。

2. USER_INDEX： 存放用户数据的索引。

3. LOB_DATA： 存放大型对象文件，包括商品图片、订单明细等。

4. TEMP： 存放临时表空间。

   

- 表设计方案：

1. 商品信息表（Products）： 包括商品ID，名称，价格，库存等字段。

2. 订单表（Orders）： 包括订单ID，用户ID，订单时间，订单状态等字段。

3. 用户信息表（Users）： 包括用户ID，用户名，密码，联系方式等字段。

4. 地址信息表（Addresses）： 包括地址ID，用户ID，联系人，联系地址等字段。

5. 订单详情表（OrderDetails）：包括订单详情ID、订单ID、商品ID、商品数量。

   

- 权限及用户分配方案：

1. 创建两个用户分别为SELLER和BUYER。

2. SELLER用户具有商品信息表、订单表和地址信息表的读写权限，具有创建存储过程和函数的权限。

3. BUYER用户具有订单表和地址信息表的读写权限。

   

- 程序包设计方案：

  创建一个程序包，包含存储过程和函数，实现以下业务逻辑：

1.  创建订单的存储过程： 输入用户ID、商品ID和订单数量，更新商品信息表的库存，插入订单表的订单信息。
2.  取消订单的存储过程： 输入订单ID，根据订单状态判断是否可以取消，更新订单表的订单状态和商品信息表的库存。
3.  查询订单的函数： 输入用户ID和订单ID，输出订单的详细信息以及商品信息。



- 备份方案设计：

1. 定期进行全量备份，至少每周一次，备份文件存放在独立的存储介质中。
2. 每天进行增量备份，备份文件存放在独立的存储介质中。
3. 定期进行系统日志备份，以保证备份的完整性。
4. 恢复数据时需按照全量备份和增量备份顺序进行恢复。

## 相应代码

- 增加表空间USER_DATA（初始大小为10M，不允许自动增加）

  ```sql
  $sqlplus system/123@pdborcl
  
  SQL>CREATE TABLESPACE USER_DATA DATAFILE
  '/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_USER_DATA_1.dbf'
    SIZE 10M AUTOEXTEND OFF
  EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
  
  --使用linux命令ls查看数据文件
  SQL>!ls /home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_USER_DATA_1.dbf -lh
  ```

  

- 增加表空间USER_INDEX：

  ```sql
  $sqlplus system/123@pdborcl
  
  SQL>CREATE TABLESPACE USER_INDEX DATAFILE
  '/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_USER_INDEX_1.dbf'
    SIZE 10M AUTOEXTEND OFF
  EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
  
  --使用linux命令ls查看数据文件
  SQL>!ls /home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_USER_INDEX_1.dbf -lh
  ```

  

- 增加表空间LOB_DATA：

  ```sql
  $sqlplus system/123@pdborcl
  
  SQL>CREATE TABLESPACE LOB_DATA DATAFILE
  '/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_LOB_DATA_1.dbf'
    SIZE 10M AUTOEXTEND OFF
  EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
  
  --使用linux命令ls查看数据文件
  SQL>!ls /home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_LOB_DATA_1.dbf -lh
  ```

  

- 增加表空间TEMP：

  ```sql
  $sqlplus system/123@pdborcl
  
  SQL>CREATE TABLESPACE TEMP DATAFILE
  '/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_TEMP_1.dbf'
    SIZE 10M AUTOEXTEND OFF
  EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
  
  --使用linux命令ls查看数据文件
  SQL>!ls /home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_TEMP_1.dbf -lh
  ```

- 创建表：

```sql
--商品信息表
CREATE TABLE Products (
    ProductID NUMBER(10) PRIMARY KEY,
    Name VARCHAR2(50),
    Price NUMBER(10,2),
    Inventory NUMBER(10)
) TABLESPACE USER_DATA;

--订单表
CREATE TABLE Orders (
    OrderID NUMBER(10) PRIMARY KEY,
    UserID NUMBER(10),
    OrderTime DATE,
    OrderState NUMBER(2)
) TABLESPACE USER_DATA;

--用户信息表
CREATE TABLE Users (
    UserID NUMBER(10) PRIMARY KEY,
    UserName VARCHAR2(50),
    Password VARCHAR2(50),
    Contact VARCHAR2(50)
) TABLESPACE USER_DATA;

--地址信息表
CREATE TABLE Addresses (
    AddressID NUMBER(10) PRIMARY KEY,
    UserID NUMBER(10),
    ContactName VARCHAR2(50),
    Address VARCHAR2(100)
) TABLESPACE USER_DATA;

-- 订单详情表
CREATE TABLE OrderDetails (
OrderDetailID NUMBER(10) PRIMARY KEY,
OrderID NUMBER(10),
ProductID NUMBER(10),
Quantity NUMBER(10)
) TABLESPACE USER_DATA;
```



- 创建用户和用户权限：

```sql
-- 创建SELLER用户
CREATE USER SELLER IDENTIFIED BY password1 DEFAULT TABLESPACE USER_DATA TEMPORARY TABLESPACE TEMP;

-- 给SELLER赋予商品信息表、订单表和地址信息表的读写权限
GRANT INSERT, SELECT, UPDATE, DELETE ON Products TO SELLER;
GRANT INSERT, SELECT, UPDATE, DELETE ON Orders TO SELLER;
GRANT INSERT, SELECT, UPDATE, DELETE ON Addresses TO SELLER;

-- 允许SELLER创建存储过程和函数
GRANT CREATE PROCEDURE TO SELLER;
GRANT CREATE FUNCTION TO SELLER;

-- 创建BUYER用户
CREATE USER BUYER IDENTIFIED BY password2 DEFAULT TABLESPACE USER_DATA TEMPORARY TABLESPACE TEMP;

-- 给BUYER赋予订单表和地址信息表的读写权限
GRANT INSERT, SELECT, UPDATE, DELETE ON Orders TO BUYER;
GRANT INSERT, SELECT, UPDATE, DELETE ON Addresses TO BUYER;
```



- 创建存储过程和函数：

```sql
-- 创建订单的存储过程
CREATE OR REPLACE PROCEDURE CreateOrder
(
    iUserID IN NUMBER,
    iProductID IN NUMBER,
    iOrderQuantity IN NUMBER
)
AS
BEGIN
    -- 更新商品信息表
    UPDATE Products SET Inventory = Inventory - iOrderQuantity WHERE ProductID = iProductID;
    
    -- 插入订单信息
    INSERT INTO Orders (OrderID, UserID, OrderTime, OrderState) VALUES (1, iUserID, SYSDATE, 1);
END;

-- 取消订单的存储过程
CREATE OR REPLACE PROCEDURE CancelOrder
(
    iOrderID IN NUMBER
)
AS
    vOrderState NUMBER(2);
    vProductID NUMBER(10);
    vOrderQuantity NUMBER(10);
BEGIN
    -- 查询订单状态和商品信息
    SELECT OrderState INTO vOrderState FROM Orders WHERE OrderID = iOrderID;
    SELECT ProductID, Quantity INTO vProductID, vOrderQuantity FROM OrderDetails WHERE OrderID = iOrderID;
    
    -- 判断订单是否可取消并更新订单状态和商品信息
    IF vOrderState = 2 THEN
        UPDATE Orders SET OrderState = 3 WHERE OrderID = iOrderID;
        UPDATE Products SET Inventory = Inventory + vOrderQuantity WHERE ProductID = vProductID;
    END IF;
END;

-- 查询订单的函数
CREATE OR REPLACE FUNCTION OrderDetail
(
    iUserID IN NUMBER,
    iOrderID IN NUMBER
) RETURN VARCHAR2
AS
    vOrderDetail VARCHAR2(500);
BEGIN
    -- 查询订单信息和商品信息
    SELECT o.OrderID, o.OrderTime, p.Name, d.Quantity, p.Price, p.Price * d.Quantity
    INTO vOrderDetail
    FROM Orders o
    JOIN OrderDetails d ON o.OrderID = d.OrderID
    JOIN Products p ON d.ProductID = p.ProductID
    WHERE o.UserID = iUserID AND o.OrderID = iOrderID;
    RETURN vOrderDetail;
END;
```



- 备份方案：

1. 全量备份：

```sql
expdp system/123 FULL=Y DIRECTORY=data_pump_dir DUMPFILE=full_backup.dmp LOGFILE=full_backup.log
```

2. 增量备份：

```sql
expdp system/123 DIRECTORY=data_pump_dir DUMPFILE=incremental_backup%U.dmp LOGFILE=incremental_backup.log SCHEMAS=SELLER,BUYER
```

3. 系统日志备份：

```sql
RMAN> CONNECT target sys/123;
RMAN> BACKUP ARCHIVELOG ALL;
```

## 模拟数据生成脚本

```sql
DECLARE
    v_product_id NUMBER(10);
    v_order_id NUMBER(10);
    v_user_id NUMBER(10);
    v_address_id NUMBER(10);
    v_details_id NUMBER(10);
BEGIN
    -- 插入商品信息
    FOR i IN 1..10000 LOOP
        v_product_id := i;
        INSERT INTO Products(ProductID, Name, Price, Inventory)
        VALUES(v_product_id, 'Product ' || i, TRUNC(DBMS_RANDOM.VALUE(10, 100), 2), TRUNC(DBMS_RANDOM.VALUE(100, 1000)));
    END LOOP;
    
    -- 插入用户信息
    FOR i IN 1..2000 LOOP
        v_user_id := i;
        INSERT INTO Users(UserID, UserName, Password, Contact)
        VALUES(v_user_id, 'User ' || i, 'password', 'contact');
    END LOOP;
    
    -- 插入地址信息
    FOR i IN 1..4000 LOOP
        v_address_id := i;
        INSERT INTO Addresses(AddressID, UserID, ContactName, Address)
        VALUES(v_address_id, TRUNC(DBMS_RANDOM.VALUE(1, 2000)), 'Address ' || i, 'Address ' || i);
    END LOOP;
    
    -- 插入订单信息和订单详细信息
    FOR i IN 1..100000 LOOP
        v_order_id := i;
        v_details_id :=i;
        INSERT INTO Orders(OrderID, UserID, OrderTime, OrderState)
        VALUES(v_order_id, TRUNC(DBMS_RANDOM.VALUE(1, 2000)), SYSDATE - TRUNC(DBMS_RANDOM.VALUE(0, 365)), TRUNC(DBMS_RANDOM.VALUE(1, 3)));
        FOR j IN 1..TRUNC(DBMS_RANDOM.VALUE(1, 5)) LOOP
            INSERT INTO OrderDetails(OrderDetailID,OrderID, ProductID, Quantity)
            VALUES(v_details_id,v_order_id, TRUNC(DBMS_RANDOM.VALUE(1, 10000)), TRUNC(DBMS_RANDOM.VALUE(1, 10)));
        END LOOP;  
    END LOOP;
END;
/
```

## 实验结果

![](https://gitlab.com/beebab/oracle/-/raw/main/test6/a.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test6/b.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test6/c.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test6/d.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test6/e.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test6/f.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test6/f.png)

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

