班级：20级软件工程3班    学号：202010414317      姓名：唐玲玲

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 参考

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

- 教材中的查询语句：查询两个部门('IT'和'Sales')的部门总人数和平均工资，两个查询的结果是一样的。但效率不相同。

查询1：

```SQL
$sqlplus hr/123@localhost/pdborcl

set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
Note
-----
   - this is an adaptive plan


统计信息
-------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	 10  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

```

- 查询2

```SQL
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")


统计信息
------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	  9  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed
```

## 实验SQL代码

● 查询每个员工的员工号、姓名、员工的管理员号，并且查询出管理员的first_name，把这个管理员的first_name命名为manager_name.

● 查询1：

```sql
SQL> SET AUTOTRACE ON
SQL> SELECT e.EMPLOYEE_ID,e.FIRST_NAME,e.MANAGER_ID,(SELECT M.FIRST_NAME FROM employees m WHERE m.EMPLOYEE_ID=e.MANAGER_ID) AS MANAGER_NAME FROM hr.employees e ORDER BY e.EMPLOYEE_ID;

输出结果：
EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	100 Steven
	101 Neena			100 Steven
	102 Lex 			100 Steven
	103 Alexander			102 Lex
	104 Bruce			103 Alexander
	105 David			103 Alexander
	106 Valli			103 Alexander
	107 Diana			103 Alexander
	108 Nancy			101 Neena
	109 Daniel			108 Nancy
	110 John			108 Nancy

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	111 Ismael			108 Nancy
	112 Jose Manuel 		108 Nancy
	113 Luis			108 Nancy
	114 Den 			100 Steven
	115 Alexander			114 Den
	116 Shelli			114 Den
	117 Sigal			114 Den
	118 Guy 			114 Den
	119 Karen			114 Den
	120 Matthew			100 Steven
	121 Adam			100 Steven

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	122 Payam			100 Steven
	123 Shanta			100 Steven
	124 Kevin			100 Steven
	125 Julia			120 Matthew
	126 Irene			120 Matthew
	127 James			120 Matthew
	128 Steven			120 Matthew
	129 Laura			121 Adam
	130 Mozhe			121 Adam
	131 James			121 Adam
	132 TJ				121 Adam

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	133 Jason			122 Payam
	134 Michael			122 Payam
	135 Ki				122 Payam
	136 Hazel			122 Payam
	137 Renske			123 Shanta
	138 Stephen			123 Shanta
	139 John			123 Shanta
	140 Joshua			123 Shanta
	141 Trenna			124 Kevin
	142 Curtis			124 Kevin
	143 Randall			124 Kevin

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	144 Peter			124 Kevin
	145 John			100 Steven
	146 Karen			100 Steven
	147 Alberto			100 Steven
	148 Gerald			100 Steven
	149 Eleni			100 Steven
	150 Peter			145 John
	151 David			145 John
	152 Peter			145 John
	153 Christopher 		145 John
	154 Nanette			145 John

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	155 Oliver			145 John
	156 Janette			146 Karen
	157 Patrick			146 Karen
	158 Allan			146 Karen
	159 Lindsey			146 Karen
	160 Louise			146 Karen
	161 Sarath			146 Karen
	162 Clara			147 Alberto
	163 Danielle			147 Alberto
	164 Mattea			147 Alberto
	165 David			147 Alberto

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	166 Sundar			147 Alberto
	167 Amit			147 Alberto
	168 Lisa			148 Gerald
	169 Harrison			148 Gerald
	170 Tayler			148 Gerald
	171 William			148 Gerald
	172 Elizabeth			148 Gerald
	173 Sundita			148 Gerald
	174 Ellen			149 Eleni
	175 Alyssa			149 Eleni
	176 Jonathon			149 Eleni

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	177 Jack			149 Eleni
	178 Kimberely			149 Eleni
	179 Charles			149 Eleni
	180 Winston			120 Matthew
	181 Jean			120 Matthew
	182 Martha			120 Matthew
	183 Girard			120 Matthew
	184 Nandita			121 Adam
	185 Alexis			121 Adam
	186 Julia			121 Adam
	187 Anthony			121 Adam

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	188 Kelly			122 Payam
	189 Jennifer			122 Payam
	190 Timothy			122 Payam
	191 Randall			122 Payam
	192 Sarah			123 Shanta
	193 Britney			123 Shanta
	194 Samuel			123 Shanta
	195 Vance			123 Shanta
	196 Alana			124 Kevin
	197 Kevin			124 Kevin
	198 Donald			124 Kevin

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	199 Douglas			124 Kevin
	200 Jennifer			101 Neena
	201 Michael			100 Steven
	202 Pat 			201 Michael
	203 Susan			101 Neena
	204 Hermann			101 Neena
	205 Shelley			101 Neena
	206 William			205 Shelley

已选择 107 行。


执行计划
----------------------------------------------------------
Plan hash value: 4265739349

--------------------------------------------------------------------------------
-------------

| Id  | Operation		    | Name	    | Rows  | Bytes | Cost (%CPU
)| Time     |

--------------------------------------------------------------------------------
-------------

|   0 | SELECT STATEMENT	    |		    |	107 |  1605 |	 22   (0
)| 00:00:01 |

|   1 |  TABLE ACCESS BY INDEX ROWID| EMPLOYEES     |	  1 |	 11 |	  1   (0
)| 00:00:01 |

|*  2 |   INDEX UNIQUE SCAN	    | EMP_EMP_ID_PK |	  1 |	    |	  0   (0
)| 00:00:01 |

|   3 |  TABLE ACCESS BY INDEX ROWID| EMPLOYEES     |	107 |  1605 |	  4   (0
)| 00:00:01 |

|   4 |   INDEX FULL SCAN	    | EMP_EMP_ID_PK |	107 |	    |	  1   (0
)| 00:00:01 |

--------------------------------------------------------------------------------
-------------


Predicate Information (identified by operation id):
---------------------------------------------------

   2 - access("M"."EMPLOYEE_ID"=:B1)


统计信息
----------------------------------------------------------
	  1  recursive calls
	  0  db block gets
	 46  consistent gets
	  0  physical reads
	  0  redo size
       4520  bytes sent via SQL*Net to client
	685  bytes received via SQL*Net from client
	  9  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	107  rows processed
```

● 查询2（优化后）：

```sql
SELECT e.EMPLOYEE_ID,e.FIRST_NAME,e.MANAGER_ID,m.FIRST_NAME AS MANAGER_NAME FROM employees e,employees m WHERE e.MANAGER_ID=m.EMPLOYEE_ID(+) ORDER BY e.EMPLOYEE_ID;

运行结果：
EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	100 Steven
	101 Neena			100 Steven
	102 Lex 			100 Steven
	103 Alexander			102 Lex
	104 Bruce			103 Alexander
	105 David			103 Alexander
	106 Valli			103 Alexander
	107 Diana			103 Alexander
	108 Nancy			101 Neena
	109 Daniel			108 Nancy
	110 John			108 Nancy

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	111 Ismael			108 Nancy
	112 Jose Manuel 		108 Nancy
	113 Luis			108 Nancy
	114 Den 			100 Steven
	115 Alexander			114 Den
	116 Shelli			114 Den
	117 Sigal			114 Den
	118 Guy 			114 Den
	119 Karen			114 Den
	120 Matthew			100 Steven
	121 Adam			100 Steven

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	122 Payam			100 Steven
	123 Shanta			100 Steven
	124 Kevin			100 Steven
	125 Julia			120 Matthew
	126 Irene			120 Matthew
	127 James			120 Matthew
	128 Steven			120 Matthew
	129 Laura			121 Adam
	130 Mozhe			121 Adam
	131 James			121 Adam
	132 TJ				121 Adam

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	133 Jason			122 Payam
	134 Michael			122 Payam
	135 Ki				122 Payam
	136 Hazel			122 Payam
	137 Renske			123 Shanta
	138 Stephen			123 Shanta
	139 John			123 Shanta
	140 Joshua			123 Shanta
	141 Trenna			124 Kevin
	142 Curtis			124 Kevin
	143 Randall			124 Kevin

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	144 Peter			124 Kevin
	145 John			100 Steven
	146 Karen			100 Steven
	147 Alberto			100 Steven
	148 Gerald			100 Steven
	149 Eleni			100 Steven
	150 Peter			145 John
	151 David			145 John
	152 Peter			145 John
	153 Christopher 		145 John
	154 Nanette			145 John

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	155 Oliver			145 John
	156 Janette			146 Karen
	157 Patrick			146 Karen
	158 Allan			146 Karen
	159 Lindsey			146 Karen
	160 Louise			146 Karen
	161 Sarath			146 Karen
	162 Clara			147 Alberto
	163 Danielle			147 Alberto
	164 Mattea			147 Alberto
	165 David			147 Alberto

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	166 Sundar			147 Alberto
	167 Amit			147 Alberto
	168 Lisa			148 Gerald
	169 Harrison			148 Gerald
	170 Tayler			148 Gerald
	171 William			148 Gerald
	172 Elizabeth			148 Gerald
	173 Sundita			148 Gerald
	174 Ellen			149 Eleni
	175 Alyssa			149 Eleni
	176 Jonathon			149 Eleni

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	177 Jack			149 Eleni
	178 Kimberely			149 Eleni
	179 Charles			149 Eleni
	180 Winston			120 Matthew
	181 Jean			120 Matthew
	182 Martha			120 Matthew
	183 Girard			120 Matthew
	184 Nandita			121 Adam
	185 Alexis			121 Adam
	186 Julia			121 Adam
	187 Anthony			121 Adam

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	188 Kelly			122 Payam
	189 Jennifer			122 Payam
	190 Timothy			122 Payam
	191 Randall			122 Payam
	192 Sarah			123 Shanta
	193 Britney			123 Shanta
	194 Samuel			123 Shanta
	195 Vance			123 Shanta
	196 Alana			124 Kevin
	197 Kevin			124 Kevin
	198 Donald			124 Kevin

EMPLOYEE_ID FIRST_NAME		 MANAGER_ID MANAGER_NAME
----------- -------------------- ---------- --------------------
	199 Douglas			124 Kevin
	200 Jennifer			101 Neena
	201 Michael			100 Steven
	202 Pat 			201 Michael
	203 Susan			101 Neena
	204 Hermann			101 Neena
	205 Shelley			101 Neena
	206 William			205 Shelley

已选择 107 行。


执行计划
----------------------------------------------------------
Plan hash value: 2948531109

--------------------------------------------------------------------------------
-------------

| Id  | Operation		 | Name 	    | Rows  | Bytes | Cost (%CPU
)| Time     |

--------------------------------------------------------------------------------
-------------

|   0 | SELECT STATEMENT	 |		    |	107 |  2782 |	  6  (17
)| 00:00:01 |

|   1 |  SORT ORDER BY		 |		    |	107 |  2782 |	  6  (17
)| 00:00:01 |

|*  2 |   HASH JOIN OUTER	 |		    |	107 |  2782 |	  5   (0
)| 00:00:01 |

|   3 |    TABLE ACCESS FULL	 | EMPLOYEES	    |	107 |  1605 |	  3   (0
)| 00:00:01 |

|   4 |    VIEW 		 | index$_join$_002 |	107 |  1177 |	  2   (0
)| 00:00:01 |

|*  5 |     HASH JOIN		 |		    |	    |	    |
 |	    |

|   6 |      INDEX FAST FULL SCAN| EMP_EMP_ID_PK    |	107 |  1177 |	  1   (0
)| 00:00:01 |

|   7 |      INDEX FAST FULL SCAN| EMP_NAME_IX	    |	107 |  1177 |	  1   (0
)| 00:00:01 |

--------------------------------------------------------------------------------
-------------


Predicate Information (identified by operation id):
---------------------------------------------------

   2 - access("E"."MANAGER_ID"="M"."EMPLOYEE_ID"(+))
   5 - access(ROWID=ROWID)


统计信息
----------------------------------------------------------
	  1  recursive calls
	  0  db block gets
	 13  consistent gets
	  3  physical reads
	  0  redo size
       4520  bytes sent via SQL*Net to client
	685  bytes received via SQL*Net from client
	  9  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	107  rows processed
```

## 实验分析

- 查询一的SQL语句的consistent gets=45，costs成本=21，它并不是最有效率的语句，原因是用了子查询语句作为一个字段属性，使得没输出employees的一行都要再次查询一次该表，还可以对语句进行优化。
- 查询二的SQL语句使用多表外连接方式查询。consistent gets=13，相比于查询一，查询二的执行效率更高。

## 实验注意事项

- 完成时间：2023-3-21，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test1目录中。
- 实验分析及结果文档说明书用Markdown格式编写。

