# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

- 使用sql-developer软件创建表，并导出类似以下的脚本。

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/a.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/b.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/c.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/d.png)

- 创建order_details表
- 创建序列SEQ1的语句

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/d.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/e.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/f.png)



- 插入100条orders记录的样例脚本如下：

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/f.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/g.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test3/h.png)

## 

