# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```



## 脚本代码

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![](https://gitlab.com/beebab/oracle/-/raw/main/test5/a.png)

## 测试

```sh

函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;


过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

```

![](https://gitlab.com/beebab/oracle/-/raw/main/test5/b.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test5/c.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test5/d.png)

![](https://gitlab.com/beebab/oracle/-/raw/main/test5/e.png)

## 实验总结
本次实验的主要目的是练习Python的模块和函数的创建、使用，以及SQL语句的编写和游标的使用。在这个实验中，我们创建了一个名为MyPack的包，包含两个函数Get_SalaryAmount和GET_EMPLOYEES。Get_SalaryAmount函数是一个查询函数，它的参数是部门ID，通过查询员工表，可以统计每个部门的工资总额。该函数首先建立一个数据库连接，然后使用SQL语句查询员工表，获取每个部门的工资总额，并将结果返回给调用者。GET_EMPLOYEES是一个过程，它的参数是员工ID。该过程使用游标递归查询某个员工及其所有下属和子下属员工。它首先建立一个数据库连接，然后使用游标从员工表中查询某个员工及其下属员工，并将结果打印出来。接着，该过程递归调用自身，查询下属员工的下属员工，直到没有下属员工为止。通过这个实验，我掌握了Python模块和函数的创建、使用，以及SQL语句的编写和游标的使用。在实验中，我学会了如何将一些相关的函数和过程封装成一个模块，提高代码的可重用性和可维护性。此外，我也加深了对数据库操作的理解和掌握了使用游标查询数据库的技能。这次实验是一次很好的练习，帮助我巩固了Python编程和数据库操作的基础知识。
