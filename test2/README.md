# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

对于以下的对象名称con_re_role，sale

- 第1步：以system登录到pdborcl，创建角色con_re_role和用户sale，并授权和分配空间：

```sql
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 19:53:29 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 19:50:01 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> CREATE ROLE con_re_role;

角色已创建。

SQL> GRANT connect,resource,CREATE VIEW，CREATE SESSION TO con_re_role;

授权成功。

SQL> CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

用户已创建。

SQL> ALTER USER sale default TABLESPACE "USERS";

用户已更改。

SQL> ALTER USER sale QUOTA 50M ON users;

用户已更改。

SQL> GRANT con_re_role TO sale;

授权成功。

SQL> REVOKE con_re_role FROM sale;

撤销成功。

```

> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale用户访问users表空间，空间限额是50M。

- 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
[oracle@oracle1 ~]$ sqlplus sale/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 20:11:46 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.


连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> show user;
USER 为 "SALE"
SQL> SELECT * FROM session_privs;

PRIVILEGE
----------------------------------------
SET CONTAINER
CREATE INDEXTYPE
CREATE OPERATOR
CREATE TYPE
CREATE TRIGGER
CREATE PROCEDURE
CREATE SEQUENCE
CREATE VIEW
CREATE CLUSTER
CREATE TABLE
CREATE SESSION

已选择 11 行。

SQL> SELECT * FROM session_roles;

ROLE
--------------------------------------------------------------------------------
CONNECT
RESOURCE
SODA_APP
CON_RE_ROLE

SQL> CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;

表已创建。

INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;

已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
授权成功。

SQL> 
NAME
--------------------------------------------------
zhang
wang

```

- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
[oracle@oracle1 ~]$ sqlplus hr/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 20:14:30 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 2月  28 2023 03:38:23 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> SELECT * FROM sale.customers;
SELECT * FROM sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在


SQL> SELECT * FROM sale.customers_view;

NAME
--------------------------------------------------
zhang
wang

```

> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
> sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 20:16:06 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 20:10:04 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

配置文件已更改

SQL> exit
从 Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production 断开
[oracle@oracle1 ~]$ sqlplus hr/13@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 20:18:02 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


请输入用户名:  
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


请输入用户名:  
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


SP2-0157: 在 3 次尝试之后无法连接到 ORACLE, 退出 SQL*Plus
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 20:18:24 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 20:16:06 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> alter user sale  account unlock;

用户已更改。
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_re_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES

SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
  8   where  a.tablespace_name = b.tablespace_name;

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      360    20.0625   339.9375      94.43
UNDOTBS1			      100    52.3125	47.6875      47.69
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      250     5.4375   244.5625      97.83

```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```sh
SQL> drop role con_re_role;

角色已删除。

SQL> drop user sale cascade;

SQL> 用户已删除。
```

## 实验

- sqldeveloper授权对象的操作界面：
![](https://gitlab.com/beebab/oracle/-/raw/main/test2/b.png)

